﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Drawing;
using System.Linq;

using UColor = UnityEngine.Color;

public class ImageProcessor : MonoBehaviour
{
    [System.Serializable]
    public struct ToleratedValue
    {
        public ToleratedValue(int v, int t) {
            value = v;
            tolerance = t;
        }

        public int value { get; set; }
        public int tolerance { get; set; }
    };

    public static Color32 white    = new Color32(0xff, 0xff, 0xff, 0xff);
    public static Color32 black    = new Color32(0, 0, 0, 0xff);
    public static Color32 green    = new Color32(0, 0xff, 0, 0xff);
    public static Color32 blue     = new Color32(0, 0, 0xff, 0xff);

    private static int H = 0;
    private static int S = 1;
    private static int V = 2;

    public static int HModulus = 360;
    public static int SModulus = 100;


    public static void FlipHorisontally(ref Color32[] image, ref Vector2Int size)
    {
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x / 2; x++)
            {
                SwapPixels(ref image, ref size, new Vector2Int(x, y), new Vector2Int(size.x - x - 1, y));
            }
        }
    }

    public static void SwapPixels(ref Color32[] image, ref Vector2Int size, Vector2Int px1, Vector2Int px2)
    {
        Color32 tmp = GetPixel(ref image, ref size, px1);
        GetPixel(ref image, ref size, px1) = GetPixel(ref image, ref size, px2);
        GetPixel(ref image, ref size, px2) = tmp;
    }

    public static ref Color32 GetPixel(ref Color32[] image, ref Vector2Int size, Vector2Int pixelPos)
    {
        return ref image[size.x * pixelPos.y + pixelPos.x];
    }
    // HSV
    public static void MakeHSVThresholdMask(ref Color32[] image, ref Vector2Int size, ref Color32[] mask, ToleratedValue[] hsvBlueFilters, ToleratedValue[] hsvGreenFilters)
    {
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                Vector2Int pixelPos = new Vector2Int(x, y);
                UColor currentPixelValue = new UColor(
                    GetPixel(ref image, ref size, pixelPos).r,
                    GetPixel(ref image, ref size, pixelPos).g,
                    GetPixel(ref image, ref size, pixelPos).b,
                    GetPixel(ref image, ref size, pixelPos).a
                );

                float h, s, v;
                UColor.RGBToHSV(currentPixelValue, out h, out s, out v);

                int iH = (int) (ClipToRange(h, 0f, 1f) * HModulus);
                int iS = (int) (ClipToRange(s, 0f, 1f) * SModulus);
                int iV = (int) (ClipToRange(v, 0, 100));

                bool hInRangeB = ModuloDistance(iH, hsvBlueFilters[H].value, HModulus) <= hsvBlueFilters[H].tolerance;
                bool sInRangeB = Mathf.Abs(iS - hsvBlueFilters[S].value) <= hsvBlueFilters[S].tolerance;
                bool vInRangeB = Mathf.Abs(iV - hsvBlueFilters[V].value) <= hsvBlueFilters[V].tolerance;

                if (hInRangeB && sInRangeB && vInRangeB)
                {
                    GetPixel(ref mask, ref size, new Vector2Int(x, y)) = blue;
                    continue;
                }

                bool hInRangeG = ModuloDistance(iH, hsvGreenFilters[H].value, HModulus) <= hsvGreenFilters[H].tolerance;
                bool sInRangeG = Mathf.Abs(iS - hsvGreenFilters[S].value) <= hsvGreenFilters[S].tolerance;
                bool vInRangeG = Mathf.Abs(iV - hsvGreenFilters[V].value) <= hsvGreenFilters[V].tolerance;

                if (hInRangeG && sInRangeG && vInRangeG)
                {
                    GetPixel(ref mask, ref size, new Vector2Int(x, y)) = green;
                    continue;
                }

                GetPixel(ref mask, ref size, new Vector2Int(x, y)) = black;
            }
        }
    }

    public static int ModuloDistance(int l, int r, int mod)
    {
        int greatestModulusValue = mod - 1;

        int lModule = l % mod;
        int rModule = r % mod;

        if (lModule - rModule < 0)
            return rModule - lModule;
        else
            return lModule - rModule;
    }

    static int ClipToRange(int value, int from, int to)
    {
        if (value < from)
            return from;
        else if (value > to)
            return to;
        return value;
    }

    static float ClipToRange(float value, float from, float to)
    {
        if (value < from)
            return from;
        else if (value > to)
            return to;
        return value;
    }

    // position

    public static List<Vector2Int> Get2DPositions(ref Color32[] image, ref Vector2Int size, List<Color32> markerToLookFor)
    {
        int numMarkers = markerToLookFor.Count;
        List<bool> markersFound = Enumerable.Repeat<bool>(false, numMarkers).ToList();
        List<Vector2Int> markerPos = Enumerable.Repeat<Vector2Int>(new Vector2Int(-1, -1), numMarkers).ToList();

        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                Vector2Int pixelPos = new Vector2Int(x, y);
                for (int m = 0; m < numMarkers; m++)
                {
                    if (!markersFound[m])
                    {
                        var currentPixel = GetPixel(ref image, ref size, pixelPos);

                        if (
                            currentPixel.r == markerToLookFor[m].r
                            && currentPixel.g == markerToLookFor[m].g
                            && currentPixel.b == markerToLookFor[m].b
                        )
                        {
                            markerPos[m] = pixelPos;
                            markersFound[m] = true;
                        }
                    }
                }
            }
        }

        return markerPos;
    }

    public static void DrawNonOverlapCircle(ref Color32[] image, ref Vector2Int size, Vector2Int pos, Color32 circleColour)
    {

    }
}
