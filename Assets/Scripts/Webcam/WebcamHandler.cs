﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcamHandler : MonoBehaviour
{
    private static WebCamTexture webCamTexture;
    private static Vector2Int frameSize;
    private static Color32[] rawCameraFrame;
    private static Texture2D processedTexture;
    private GameObject wand;
    private GameObject wandAnchor;
    public string wandObjName = "Wand";
    public string wandAnchorName = "WandAnchor";
    public Vector3 wandStartOffset = new Vector3(0, 0, 0);
    public float cameraSize = .4f;
    public bool rawCameraView = false;
    public bool drawMarkersMoveRanges = true;
    public int bluePositionChangeDetectionRange = 30;
    public int greenPositionChangeDetectionRange = 16;
    public Vector2Int blueMarkerPosition = new Vector2Int(0,0);
    public Vector2Int greenMarkerPosition = new Vector2Int(0,0);

    [Space(10)]
    public Vector3 vievboxRanges = new Vector3(1f, 1f, 1f);
    public float maxMarkersDistance = 1f;

    [Space(10)]
    public int hVb = 0;
    public int hTb = 0;
    public int sVb = 0;
    public int sTb = 0;
    public int vVb = 0;
    public int vTb = 0;
    [Space(10)]
    public int hVg = 0;
    public int hTg = 0;
    public int sVg = 0;
    public int sTg = 0;
    public int vVg = 0;
    public int vTg = 0;

    // Start is called before the first frame update
    void Start()
    {
        webCamTexture = new WebCamTexture(320, 240, 60);
        webCamTexture.Play();
        frameSize = new Vector2Int(webCamTexture.width, webCamTexture.height);

        rawCameraFrame = new Color32[frameSize.x * frameSize.y];

        processedTexture = new Texture2D(frameSize.x, frameSize.y);
        GetComponent<Image>().material.mainTexture = processedTexture;

        GetComponent<RectTransform>().anchoredPosition = new Vector3(1f, 1f, 0);

        wand = GameObject.Find(wandObjName);
        wandAnchor = GameObject.Find(wandAnchorName);
        wand.transform.position = wandStartOffset;
    }

    // Update is called once per frame
    void Update()
    {
        ClipToRange(ref cameraSize, 0f, 1000f);
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)(frameSize.x) * cameraSize);
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (float)(frameSize.y) * cameraSize);

        webCamTexture.GetPixels32(rawCameraFrame);
        ImageProcessor.FlipHorisontally(ref rawCameraFrame, ref frameSize);

        ClipToRange(ref hVb, 0, 360);
        ClipToRange(ref sVb, 0, 100);
        ClipToRange(ref vVb, 0, 100);
        ClipToRange(ref hTb, 0, 360);
        ClipToRange(ref sTb, 0, 100);
        ClipToRange(ref vTb, 0, 100);

        ClipToRange(ref hVg, 0, 360);
        ClipToRange(ref sVg, 0, 100);
        ClipToRange(ref vVg, 0, 100);
        ClipToRange(ref hTg, 0, 360);
        ClipToRange(ref sTg, 0, 100);
        ClipToRange(ref vTg, 0, 100);

        if (!rawCameraView)
            ProcessFrame(ref rawCameraFrame);

        UpdateCameraPreview(ref rawCameraFrame);
        TransformWandPosition();
    }

    private static Vector3 rotationAxis = new Vector3(0, 0, 1f);
    private static Vector3 leanAxis = new Vector3(1f, 0, 0);
    void TransformWandPosition()
    {
        const int b = 0;
        const int g = 1;
        List<Vector2> normalisedPostition = new List<Vector2>()
        {
            new Vector2((float)blueMarkerPosition.x/frameSize.x, (float)blueMarkerPosition.y/frameSize.y),
            new Vector2((float)greenMarkerPosition.x/frameSize.x, (float)greenMarkerPosition.y/frameSize.y)
        };

        wand.transform.localRotation = (
            Quaternion.AngleAxis(GetTransformAngle(normalisedPostition[g], normalisedPostition[b]), rotationAxis)
            * Quaternion.AngleAxis(GetLeanAngle(normalisedPostition[g], normalisedPostition[b]), leanAxis)
        );

        wandAnchor.transform.position = new Vector3(
            wandStartOffset.x + vievboxRanges.x * (normalisedPostition[b].x - 0.5f),
            wandStartOffset.y + vievboxRanges.y * (normalisedPostition[b].y - 0.5f),
            wandStartOffset.z + vievboxRanges.z
        );

        /*wand.transform.localEulerAngles = new Vector3(
            0,
            0,
            GetTransformAngle(normalisedPostition[g], normalisedPostition[b])
        );

        wand.transform.position = new Vector3(
            wandStartOffset.x + vievboxRanges.x * (normalisedPostition[b].x - 0.5f),
            wandStartOffset.y + vievboxRanges.y * (normalisedPostition[b].y - 0.5f),
            wandStartOffset.z + vievboxRanges.z
        );*/
        /*
        wand.transform.rotation = (
            Quaternion.AngleAxis(GetTransformAngle(normalisedPostition[g], normalisedPostition[b]), rotationAxis)
            //* Quaternion.AngleAxis(GetTransformAngle(normalisedPostition[g], normalisedPostition[b]), leanAxis)
        );*/
    }

    float GetTransformAngle(Vector2 g, Vector2 b)
    {
        return Mathf.Rad2Deg * Mathf.Atan2(g.y - b.y, g.x - b.x) - 90;
    }

    float GetLeanAngle(Vector2 g, Vector2 b)
    {
        float normalisedDistance = Vector2.Distance(g, b) / maxMarkersDistance;
        
        return 90 - 90 * normalisedDistance;
    }

    void UpdateCameraPreview(ref Color32[] frame)
    {
        processedTexture.SetPixels32(frame);
        processedTexture.Apply();
    }

    void ProcessFrame(ref Color32[] frame)
    {
        ImageProcessor.MakeHSVThresholdMask(ref frame, ref frameSize, ref frame,
            new ImageProcessor.ToleratedValue[3] { new ImageProcessor.ToleratedValue(hVb, hTb), new ImageProcessor.ToleratedValue(sVb, sTb), new ImageProcessor.ToleratedValue(vVb, vTb) },
            new ImageProcessor.ToleratedValue[3] { new ImageProcessor.ToleratedValue(hVg, hTg), new ImageProcessor.ToleratedValue(sVg, sTg), new ImageProcessor.ToleratedValue(vVg, vTg) }
        );

        List<Vector2Int> markersPos = ImageProcessor.Get2DPositions(ref frame, ref frameSize,
            new List<Color32>() { ImageProcessor.green, ImageProcessor.blue }
        );

        if (markersPos[0].x >= 0 && markersPos[0].y >= 0)
            greenMarkerPosition = markersPos[0];
        if (markersPos[1].x >= 0 && markersPos[1].y >= 0)
            blueMarkerPosition = markersPos[1];

        if (drawMarkersMoveRanges)
        {
            ImageProcessor.DrawNonOverlapCircle(ref frame, ref frameSize, greenMarkerPosition, ImageProcessor.white);
            ImageProcessor.DrawNonOverlapCircle(ref frame, ref frameSize, blueMarkerPosition, ImageProcessor.white);
        }


    }

    void ClipToRange(ref int value, int from, int to)
    {
        if (value < from)
            value = from;
        else if (value > to)
            value = to;
    }

    void ClipToRange(ref float value, float from, float to)
    {
        if (value < from)
            value = from;
        else if (value > to)
            value = to;
    }
}
